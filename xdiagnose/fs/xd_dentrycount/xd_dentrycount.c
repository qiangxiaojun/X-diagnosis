#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/sysmacros.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <getopt.h>
#include <asm/types.h>
#include <stdarg.h>
#include <sys/resource.h>
#include <signal.h>
#include <sys/time.h>


/* for bpf*/
#include <linux/bpf.h>
#include <bpf/bpf.h>
/*  bpf end */

#include "xd_dentrycount.h"
#include "common_u.h"
#include "xd_dentrycount.skel.h"

#define ARG_BASE_10 10

#define MIN_LIMIT_INTERVAL_S 1
#define MAX_LIMIT_INTERVAL_S 10

#define MIN_DENTRY_NUM_RECORD 1
#define DEFAULT_DENTRY_NUM_RECORD 50
#define MAX_DENTRY_NUM_RECORD 0x7fffffff

static bool running = true;
static int dir_mapfd;
static int args_mapfd;
static long interval_time = MIN_LIMIT_INTERVAL_S;
static long num_record = DEFAULT_DENTRY_NUM_RECORD;

static const struct option long_opts[] = {
	{ "help", 0, 0, 'h' },
	{ "num", 1, 0, 'n' },
	{ "interval", 1, 0, 'i' },
	{ 0 }
};

static void usage(char *cmd)
{
	printf("Usage: %s [ OPTIONS ]\n"
			"   -h,--help\t\tthis message\n"
			"   -n,--num\t\tthreshold record number\n"
			"   -i,--interval\tinterval time (1-10)s\n", cmd);
}

static void xarp_sig_handler(int sig)
{
	switch(sig){
		case SIGALRM:
			break;
		case SIGTERM:
		case SIGINT:
			running = false;
			break;
		default:
			break;
	}
}

static void dentry_memchk_show(void)
{
	int ret;
	long key, next_key;
	struct dir_dentryinfo value;

	key = 0;
	while(bpf_map_get_next_key(dir_mapfd, &key, &next_key) == 0){
		ret = bpf_map_lookup_elem(dir_mapfd, &next_key, &value);
		if(ret != 0){
			printf("stack_mapfd: bpf_map_lookup_elem failed\n");
			continue;
		}

		printf("  dev:%s, dir:%s, sub dentry:%u, dentry negative:%u\n", \
			value.devname, value.dirname, value.num_sub, value.num_neg);
		key = next_key;
	}

	key = 0;
	while(bpf_map_get_next_key(dir_mapfd, &key, &next_key) == 0){
		bpf_map_delete_elem(dir_mapfd, &next_key);
		key = next_key;
	}

	return;
}

static int dentry_memchk(void)
{
	int ret;
	int arg_key, arg_val;
	long key, next_key;
	struct itimerval itv_new;

	arg_key = 0;
	arg_val = (int)num_record;
	printf("threshold record number:%u\n", arg_val);
	ret = bpf_map_update_elem(args_mapfd, &arg_key, &arg_val, BPF_ANY);
	if (ret < 0){
		printf("config bpf args failed\n");
		return ret;
	}

	itv_new.it_value.tv_sec = interval_time;
	itv_new.it_value.tv_usec = 0;
	itv_new.it_interval.tv_sec = interval_time;
	itv_new.it_interval.tv_usec = 0;
	signal(SIGALRM, xarp_sig_handler);

	key = 0;
	while(bpf_map_get_next_key(dir_mapfd, &key, &next_key) == 0){
		bpf_map_delete_elem(dir_mapfd, &next_key);
		key = next_key;
	}

	setitimer(ITIMER_REAL, &itv_new, NULL);
	while(running){
		pause();
		dentry_memchk_show();
	} 
	
	return 0;
}

int main(int argc, char **argv)
{
	int ch;
	int ret = 0;
	struct xd_dentrycount_bpf *skel;

	while ((ch = getopt_long(argc, argv, "hn:i:", long_opts, NULL)) != -1) {
		switch (ch) {
		case 'i':
			interval_time = strtol(optarg, NULL, ARG_BASE_10);
			if (errno || interval_time < MIN_LIMIT_INTERVAL_S \
				|| interval_time > MAX_LIMIT_INTERVAL_S) {
				printf("Invalid argument -i %s\n", optarg);
				return -1;
			}
			break;
		case 'n':
			num_record = strtol(optarg, NULL, ARG_BASE_10);
			if (errno || num_record < MIN_DENTRY_NUM_RECORD \
				|| num_record > MAX_DENTRY_NUM_RECORD) {
				printf("Invalid argument -n %s\n", optarg);
				return -1;
			}
			break;
		case 'h':
			usage(argv[0]);
			return 0;
		default:
			printf("Invalid argument\n");
			return -1;
		}
	}

	memlock_rlimit();

	skel = xd_dentrycount_bpf__open_and_load();
	if (!skel) {
		fprintf(stderr, "Failed to open and load BPF skeleton\n");
		ret = -1;
		goto cleanup;
	}   

	ret = xd_dentrycount_bpf__attach(skel);
	if (ret) {
		fprintf(stderr, "Failed to attach BPF skeleton\n");
		ret = -1;
		goto cleanup;
	}

	dir_mapfd = bpf_map__fd(skel->maps.dir_map);
	if (dir_mapfd < 0) {
		fprintf(stderr, "Failed to get BPF map fd\n");
		ret = -1;
		goto cleanup;
	}
	
	args_mapfd = bpf_map__fd(skel->maps.args_map);
	if (args_mapfd < 0) {
		fprintf(stderr, "Failed to get BPF map fd\n");
		ret = -1;
		goto cleanup;
	}
	
	ret = dentry_memchk();

cleanup:
	xd_dentrycount_bpf__destroy(skel);
	return ret;
}
