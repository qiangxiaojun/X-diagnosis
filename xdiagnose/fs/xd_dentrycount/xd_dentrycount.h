#ifndef __XD_DENTRY_MEMCHK_H__
#define __XD_DENTRY_MEMCHK_H__

#ifndef TASK_COMM_LEN
#define TASK_COMM_LEN 16
#endif

#define MAX_DEVNAME_LEN 32

#ifndef DNAME_INLINE_LEN
#define DNAME_INLINE_LEN 32
#endif

/* same as BPF_COMPLEXITY_LIMIT_JMP_SEQ */
#define __XD_MAX_SUBDIR_NUM 4096

struct dir_dentryinfo {
	char devname[MAX_DEVNAME_LEN];
	char dirname[DNAME_INLINE_LEN];
	unsigned int num_sub;
	unsigned int num_neg;
};

#endif
