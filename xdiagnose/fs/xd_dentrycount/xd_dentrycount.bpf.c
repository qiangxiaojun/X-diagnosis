#include "common_k.h"
#include "xd_dentrycount.h"
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>


#define ARPHRD_IEEE1394 24	  /* IEEE 1394 IPv4 - RFC 2734	*/
#define MAX_RECORD_COUNT 4096
#define FS_REQUIRES_DEV 1

struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__uint(key_size, sizeof(long));
	__uint(value_size, sizeof(struct dir_dentryinfo));
	__uint(max_entries, MAX_RECORD_COUNT);
} dir_map SEC(".maps");

struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__uint(max_entries, 1);
	__uint(key_size, sizeof(int));
	__uint(value_size, sizeof(int));
} args_map SEC(".maps");

static void dir_dentry_check(struct dentry *dir)
{
	int flags;
	int args_key, *args_value;
	struct dir_dentryinfo *value = NULL;
	struct super_block *sb;
	struct file_system_type *fstype;

	args_key = 0;
	args_value = bpf_map_lookup_elem(&args_map,&args_key);
	if(!args_value)
		return;

	bpf_probe_read((void *)&sb, sizeof(void *), &dir->d_sb);
	bpf_probe_read((void *)&fstype, sizeof(void *), &sb->s_type);
	bpf_probe_read((void *)&flags, sizeof(int), &fstype->fs_flags);
	if((flags & FS_REQUIRES_DEV) == 0)
		return;
	value = bpf_map_lookup_elem(&dir_map, &dir);
	if (!value) {
		unsigned int i;
		struct dir_dentryinfo new = {0};
		struct list_head *head, *next;
		bpf_probe_read((void *)&head, sizeof(void *), &dir->d_subdirs.next);
		next = head;
		for (i = 0; i < __XD_MAX_SUBDIR_NUM; i++) {
			bpf_probe_read((void *)&next, sizeof(void *),&next->next);
			if (next == head)
				break;
		}
		new.num_sub = i;
		bpf_probe_read_str(new.devname, MAX_DEVNAME_LEN, sb->s_id);
		bpf_probe_read((void *)&new.num_neg, sizeof(int), &dir->d_neg_dnum);
		bpf_probe_read_str(new.dirname, DNAME_INLINE_LEN, dir->d_iname);
		if (new.num_neg > *args_value || new.num_sub > *args_value)
			bpf_map_update_elem(&dir_map, &dir, &new, BPF_NOEXIST);
	}
}

SEC("kprobe/walk_component")
int bpf__walk_component(struct pt_regs *ctx)
{
	struct dentry *dir;
	struct nameidata *nd;

	nd = (struct nameidata *)PT_REGS_PARM1(ctx);
	bpf_probe_read((void *)&dir, sizeof(long), &nd->path.dentry);
	if(dir)
		dir_dentry_check(dir);

	return 0;
}

SEC("kprobe/terminate_walk")
int bpf__terminate_walk(struct pt_regs *ctx)
{
	struct dentry *dentry, *dir;
	struct nameidata *nd;

	nd = (struct nameidata *)PT_REGS_PARM1(ctx);
	bpf_probe_read((void *)&dir, sizeof(long), &nd->path.dentry);
	if(dir)
		dir_dentry_check(dir);

	return 0;
}

char _license[] SEC("license") = "GPL";
